# WordPress custom shortcode that displays a post categories.

You can use this shortcode in your posts or pages by adding [category_selector] in the editor.

You can choose the desired layout by passing the layout parameter in the shortcode, like this: [category_selector layout="list"] for the list layout and [category_selector layout="select"] for the select layout.

Also, you can exclude categories by their slug by passing the exclude parameter in the shortcode, like this: [category_selector exclude="uncategorized, drafts"]. The excluded categories are specified as a comma-separated list of slugs.

## How to use install it?
Just copy this code on your functions.php file of your theme:
```php
function category_selector_shortcode( $atts ) {
	$atts = shortcode_atts( array(
	   'layout' => 'select',
	   'exclude' => ''
	), $atts );
	$exclude_slugs = explode(',', $atts['exclude']);
	$exclude_ids = array();
	foreach ($exclude_slugs as $slug) {
	   $category = get_category_by_slug($slug);
	   if ($category) {
		  $exclude_ids[] = $category->term_id;
	   }
	}
	$categories = get_categories(array(
	   'exclude' => implode(',', $exclude_ids)
	));
	if ($atts['layout'] == 'list') {
	   $output = '<div class="category-buttons">';
	   foreach($categories as $category) {
		  $output .= '<a href="'.get_category_link($category->term_id).'" class="button">'.$category->name.'</a>';
	   }
	   $output .= '</div>';
	} else {
	   $output = '<form><select id="category-selector" name="category-selector" onchange="window.location.href=this.value;">';
	   $output .= '<option value="">'.__('Select category').'</option>';
	   foreach($categories as $category) {
		  $output .= '<option value="'.get_category_link($category->term_id).'">'.$category->name.'</option>';
	   }
	   $output .= '</select></form>';
	}
	return $output;
}
add_shortcode( 'category_selector', 'category_selector_shortcode' );

``` 
