function category_selector_shortcode( $atts ) {
	$atts = shortcode_atts( array(
	   'layout' => 'select',
	   'exclude' => ''
	), $atts );
	$exclude_slugs = explode(',', $atts['exclude']);
	$exclude_ids = array();
	foreach ($exclude_slugs as $slug) {
	   $category = get_category_by_slug($slug);
	   if ($category) {
		  $exclude_ids[] = $category->term_id;
	   }
	}
	$categories = get_categories(array(
	   'exclude' => implode(',', $exclude_ids)
	));
	if ($atts['layout'] == 'list') {
	   $output = '<div class="category-buttons">';
	   foreach($categories as $category) {
		  $output .= '<a href="'.get_category_link($category->term_id).'" class="button">'.$category->name.'</a>';
	   }
	   $output .= '</div>';
	} else {
	   $output = '<form><select id="category-selector" name="category-selector" onchange="window.location.href=this.value;">';
	   $output .= '<option value="">'.__('Select category').'</option>';
	   foreach($categories as $category) {
		  $output .= '<option value="'.get_category_link($category->term_id).'">'.$category->name.'</option>';
	   }
	   $output .= '</select></form>';
	}
	return $output;
}
add_shortcode( 'category_selector', 'category_selector_shortcode' );